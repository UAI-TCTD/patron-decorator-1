﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnArgentino = New System.Windows.Forms.Button()
        Me.btnUruguayo = New System.Windows.Forms.Button()
        Me.btnCiencia = New System.Windows.Forms.Button()
        Me.btnArte = New System.Windows.Forms.Button()
        Me.btnPolitica = New System.Windows.Forms.Button()
        Me.btnGastronomia = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnArgentino
        '
        Me.btnArgentino.Location = New System.Drawing.Point(78, 12)
        Me.btnArgentino.Name = "btnArgentino"
        Me.btnArgentino.Size = New System.Drawing.Size(156, 23)
        Me.btnArgentino.TabIndex = 0
        Me.btnArgentino.Text = "Argentino"
        Me.btnArgentino.UseVisualStyleBackColor = True
        '
        'btnUruguayo
        '
        Me.btnUruguayo.Location = New System.Drawing.Point(240, 12)
        Me.btnUruguayo.Name = "btnUruguayo"
        Me.btnUruguayo.Size = New System.Drawing.Size(156, 23)
        Me.btnUruguayo.TabIndex = 1
        Me.btnUruguayo.Text = "Uruguayo"
        Me.btnUruguayo.UseVisualStyleBackColor = True
        '
        'btnCiencia
        '
        Me.btnCiencia.Location = New System.Drawing.Point(78, 68)
        Me.btnCiencia.Name = "btnCiencia"
        Me.btnCiencia.Size = New System.Drawing.Size(75, 23)
        Me.btnCiencia.TabIndex = 2
        Me.btnCiencia.Text = "Ciencia"
        Me.btnCiencia.UseVisualStyleBackColor = True
        '
        'btnArte
        '
        Me.btnArte.Location = New System.Drawing.Point(159, 68)
        Me.btnArte.Name = "btnArte"
        Me.btnArte.Size = New System.Drawing.Size(75, 23)
        Me.btnArte.TabIndex = 3
        Me.btnArte.Text = "Arte"
        Me.btnArte.UseVisualStyleBackColor = True
        '
        'btnPolitica
        '
        Me.btnPolitica.Location = New System.Drawing.Point(240, 68)
        Me.btnPolitica.Name = "btnPolitica"
        Me.btnPolitica.Size = New System.Drawing.Size(75, 23)
        Me.btnPolitica.TabIndex = 4
        Me.btnPolitica.Text = "Politica"
        Me.btnPolitica.UseVisualStyleBackColor = True
        '
        'btnGastronomia
        '
        Me.btnGastronomia.Location = New System.Drawing.Point(321, 68)
        Me.btnGastronomia.Name = "btnGastronomia"
        Me.btnGastronomia.Size = New System.Drawing.Size(75, 23)
        Me.btnGastronomia.TabIndex = 5
        Me.btnGastronomia.Text = "Gastronomia"
        Me.btnGastronomia.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Descripcion"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(78, 41)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(318, 20)
        Me.txtDescripcion.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Nacionalidad"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Intereses"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(414, 118)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnGastronomia)
        Me.Controls.Add(Me.btnPolitica)
        Me.Controls.Add(Me.btnArte)
        Me.Controls.Add(Me.btnCiencia)
        Me.Controls.Add(Me.btnUruguayo)
        Me.Controls.Add(Me.btnArgentino)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnArgentino As System.Windows.Forms.Button
    Friend WithEvents btnUruguayo As System.Windows.Forms.Button
    Friend WithEvents btnCiencia As System.Windows.Forms.Button
    Friend WithEvents btnArte As System.Windows.Forms.Button
    Friend WithEvents btnPolitica As System.Windows.Forms.Button
    Friend WithEvents btnGastronomia As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
