﻿Public Class Usuario
    Private _descripcion As String
    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

End Class

Public Class Argentino
    Inherits Usuario

    'Public Overrides Function descripcion() As String
    '    _descripcion = "Argentino"
    'End Function
    Public Sub New()
        descripcion = "Argentino"
    End Sub
End Class

Public Class Uruguayo
    Inherits Usuario

    Public Sub New()
        MyBase.descripcion = "Uruguayo"
    End Sub

End Class

Public MustInherit Class UsuarioDecorator
    Inherits Usuario

    Private _usuario As Usuario
    Public Property usuario() As Usuario
        Get
            Return _usuario
        End Get
        Set(ByVal value As Usuario)
            _usuario = value
        End Set
    End Property

    'Dim _usuario As Usuario
    'Public Sub New(usuario As Usuario)
    '    _usuario = usuario

    'End Sub

End Class

Public Class CienciaDecorator
    Inherits UsuarioDecorator
    Public Sub New()
        'usuario.descripcion = "Ciencia"
        MyBase.descripcion = "Ciencia"
    End Sub
End Class

Public Class ArteDecorator
    Inherits UsuarioDecorator
    Public Sub New()
        MyBase.descripcion = "Arte"
    End Sub
End Class
Public Class PoliticaDecorator
    Inherits UsuarioDecorator
    Public Sub New()
        MyBase.descripcion = "Politica"
    End Sub
End Class
Public Class GastronomiaDecorator
    Inherits UsuarioDecorator
    Public Sub New()
        MyBase.descripcion = "Gastronomia"
    End Sub
End Class
