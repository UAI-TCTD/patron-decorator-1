﻿Public Class Form1

    Dim usuario As Usuario
    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub btnArgentino_Click(sender As Object, e As EventArgs) Handles btnArgentino.Click
        usuario = New Argentino
        txtDescripcion.Text = usuario.descripcion
    End Sub

    Private Sub btnUruguayo_Click(sender As Object, e As EventArgs) Handles btnUruguayo.Click
        usuario = New Uruguayo
        txtDescripcion.Text = usuario.descripcion
    End Sub

    Private Sub btnCiencia_Click(sender As Object, e As EventArgs) Handles btnCiencia.Click
        usuario = New CienciaDecorator
        txtDescripcion.Text = usuario.descripcion
    End Sub

    Private Sub btnArte_Click(sender As Object, e As EventArgs) Handles btnArte.Click
        usuario = New ArteDecorator
        txtDescripcion.Text = usuario.descripcion
    End Sub

    Private Sub btnPolitica_Click(sender As Object, e As EventArgs) Handles btnPolitica.Click
        usuario = New PoliticaDecorator
        txtDescripcion.Text = usuario.descripcion
    End Sub

    Private Sub btnGastronomia_Click(sender As Object, e As EventArgs) Handles btnGastronomia.Click
        usuario = New GastronomiaDecorator
        txtDescripcion.Text = usuario.descripcion
    End Sub
End Class